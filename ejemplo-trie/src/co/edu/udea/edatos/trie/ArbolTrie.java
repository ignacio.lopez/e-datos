package co.edu.udea.edatos.trie;

public class ArbolTrie {
    private Nodo raiz;

    public ArbolTrie() {
        raiz = new Nodo();
    }

    public void agregar(String valor) {
        Nodo nodoActual = this.raiz;
        for (int i = 0; i < valor.length(); i++) {
            char digito = valor.charAt(i);
            Nodo hijo = nodoActual.consultarDigito(digito);
            if (hijo == null) {
                Nodo nuevoHijo = new Nodo(valor);
                nodoActual.agregarHijo(digito, nuevoHijo);
                return;
            } else {
                if (hijo.esHoja()) {
                    Nodo nuevoPadre = new Nodo();
                    nodoActual.agregarHijo(digito, nuevoPadre);
                    nodoActual = nuevoPadre;
                    String valorHijo = hijo.getValor();
                    for (int j = i + 1; j < valor.length() && j < valorHijo.length(); j++) {
                        if (valor.charAt(j) == valorHijo.charAt(j)) {
                            nuevoPadre = new Nodo();
                            nodoActual.agregarHijo(valor.charAt(j), nuevoPadre);
                            nodoActual = nuevoPadre;
                        } else {
                            nodoActual.agregarHijo(valorHijo.charAt(j), hijo);
                            Nodo nuevoHijo = new Nodo(valor);
                            nodoActual.agregarHijo(valor.charAt(j), nuevoHijo);
                            break;
                        }
                    }
                    return;
                }
                nodoActual = hijo;
            }
        }
    }

    public boolean consultar(String valor) {
        Nodo nodoActual = this.raiz;
        for (int i = 0; i < valor.length(); i++) {
            Nodo hijo = nodoActual.consultarDigito(valor.charAt(i));
            if(hijo.esHoja()){
                return valor.equals(hijo.getValor());
            }
            if (hijo == null) {
                return false;
            }
            System.out.println(valor.charAt(i));
            nodoActual = hijo;
        }
        return true;
    }
}
