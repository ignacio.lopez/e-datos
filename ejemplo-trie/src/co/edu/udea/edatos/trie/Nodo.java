package co.edu.udea.edatos.trie;

import java.util.HashMap;
import java.util.Map;

public class Nodo {

    private Map<Character, Nodo> hijos;
    private boolean hoja;
    private String valor;

    public Nodo(){
        hijos = new HashMap();
        hoja = false;
        valor =  "";
    }

    public Nodo(String valor){
        hoja= true;
        this.valor = valor;
    }

    public Nodo consultarDigito(char digito){
        return hijos.get(digito);
    }

    public void agregarHijo(char digito, Nodo hijo){
        this.hijos.put(digito, hijo);
        System.out.println("Hijo agregado: "+digito);
    }

    public String getValor() {
        return valor;
    }

    public boolean esHoja(){
        return this.hoja;
    }
}
