package co.edu.udea.edatos.trie;

public class Principal {

    public static void main(String[] args) {
        ArbolTrie arbol = new ArbolTrie();
        arbol.agregar("951941654");
        arbol.agregar("562442169");
        arbol.agregar("271163624");
        arbol.agregar("278491515");

        if(arbol.consultar("278491515")){
            System.out.println("Registro encontrado");
        }else{
            System.out.println("No encontrado");
        }

    }
}
