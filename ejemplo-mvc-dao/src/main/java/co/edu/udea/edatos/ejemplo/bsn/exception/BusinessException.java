package co.edu.udea.edatos.ejemplo.bsn.exception;

public class BusinessException extends Exception {

    public BusinessException(String mensaje){
        super(mensaje);
    }
}
