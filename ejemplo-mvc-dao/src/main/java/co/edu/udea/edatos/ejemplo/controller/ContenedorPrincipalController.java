package co.edu.udea.edatos.ejemplo.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

import java.io.IOException;

public class ContenedorPrincipalController {

    @FXML
    private BorderPane contenedorPrincipal;

    public void mnuArchivoSalir_action(){
        System.exit(0);
    }

    public void mnuUsuariosRegistrar_action(){
        try {
            AnchorPane registrarUsuario = FXMLLoader.load(getClass().getClassLoader().getResource("view/registrar-usuario.fxml"));
            contenedorPrincipal.setCenter(registrarUsuario);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void mnuDireccionesRegistrar_action(){
        try {
            AnchorPane registrarDireccion = FXMLLoader.load(getClass().getClassLoader().getResource("view/registrar-direccion.fxml"));
            contenedorPrincipal.setCenter(registrarDireccion);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
