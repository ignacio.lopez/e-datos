package co.edu.udea.edatos.ejemplo.controller;

import co.edu.udea.edatos.ejemplo.bsn.UsuarioBsn;
import co.edu.udea.edatos.ejemplo.bsn.exception.BusinessException;
import co.edu.udea.edatos.ejemplo.bsn.exception.specific.CorreoInvalidoException;
import co.edu.udea.edatos.ejemplo.bsn.exception.specific.IdentificacionInvalidaException;
import co.edu.udea.edatos.ejemplo.model.Usuario;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.util.List;

public class RegistrarUsuarioController {

    @FXML
    private TextField txtIdentificacion;
    @FXML
    private TextField txtNombres;
    @FXML
    private TextField txtApellidos;
    @FXML
    private TextField txtCorreo;
    @FXML
    private TableView<Usuario> tblUsuarios;
    @FXML
    private TableColumn<Usuario, String> clmIdentificacion;
    @FXML
    private TableColumn<Usuario, String> clmNombres;


    private UsuarioBsn usuarioBsn = new UsuarioBsn();

    @FXML //postconstruct
    private void initialize() {
        //se consultan los usuarios que ya  existen
        List<Usuario> usuarios = this.usuarioBsn.listarUsuarios();
        //se crea una lista observable para hacer binding con la tabla
        ObservableList<Usuario> usuariosObservables = FXCollections.observableList(usuarios);
        //se asigna la lista observable a la tabla
        tblUsuarios.setItems(usuariosObservables);
        //se especifican los métodos que deben invocarse para obtener los valores a mostrar en las celdas
        clmIdentificacion.setCellValueFactory(cellData -> new SimpleStringProperty(Integer.toString(cellData.getValue().getIdentificacion())));
        clmNombres.setCellValueFactory(cellData -> new SimpleStringProperty(cellData.getValue().obtenerNombreCompleto()));

        tblUsuarios.getSelectionModel().selectedItemProperty()
                .addListener((observable, oldValue, newValue) -> seleccionarUsuario(newValue));

        txtIdentificacion.setTextFormatter(new TextFormatter<>(change -> {
            if (change.getControlNewText().matches("([1-9][0-9]*)?") && change.getControlText().length() < 12) {
                return change;
            }
            return null;
        }));


    }

    private void seleccionarUsuario(Usuario usuarioSeleccionado) {
        this.limpiarFormulario();
        this.txtIdentificacion.setText(String.valueOf(usuarioSeleccionado.getIdentificacion()));
        this.txtNombres.setText(usuarioSeleccionado.getNombres());
        this.txtApellidos.setText(usuarioSeleccionado.getApellidos());
        this.txtCorreo.setText(usuarioSeleccionado.getCorreo());
    }

    public void btnGuardar_clic() {
        String idIngresado = txtIdentificacion.getText();
        String nombresIngresados = txtNombres.getText();
        String apellidosIngresados = txtApellidos.getText();
        String correoIngresado = txtCorreo.getText();

        if (!correoIngresado.matches("^(.+)@(.+)$")) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Resultado");
            alert.setHeaderText("Guardar usuario");
            alert.setContentText("El formato del correo es incorrecto");
            alert.showAndWait();
            txtCorreo.requestFocus();
            return;
        }

        //TODO validar campos: tipo de dato y valores requeridos.
        int idIngresadoEntero = Integer.parseInt(idIngresado);
        Usuario usuario = new Usuario(idIngresadoEntero, nombresIngresados, apellidosIngresados, correoIngresado);
        try {
            Usuario usuarioAlmacenado = usuarioBsn.guardarUsuario(usuario);
            this.tblUsuarios.getItems().add(usuarioAlmacenado);
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Resultado");
            alert.setHeaderText("Guardar usuario");
            alert.setContentText("Usuario almacenado correctamente");
            this.limpiarFormulario();
            alert.showAndWait();
        } catch (BusinessException be) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Resultado");
            alert.setHeaderText("Guardar usuario");
            alert.setContentText(be.getMessage());
            alert.showAndWait();
            if (be instanceof IdentificacionInvalidaException) {
                txtIdentificacion.requestFocus();
            } else if (be instanceof CorreoInvalidoException) {
                txtCorreo.requestFocus();
            }
        }

    }

    public void limpiarFormulario() {
        this.txtIdentificacion.setText("");
        this.txtNombres.setText("");
        this.txtApellidos.setText("");
        this.txtCorreo.setText("");
    }

}
