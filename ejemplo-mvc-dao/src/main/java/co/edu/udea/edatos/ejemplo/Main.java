package co.edu.udea.edatos.ejemplo;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getClassLoader().getResource("view/contenedor-principal.fxml"));
        stage.setTitle("Ejemplo MVC-DAO");
        stage.setScene(new Scene(root, 400, 500));
        stage.show();
    }
}
