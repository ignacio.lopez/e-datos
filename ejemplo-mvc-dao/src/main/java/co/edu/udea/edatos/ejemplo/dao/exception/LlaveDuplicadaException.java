package co.edu.udea.edatos.ejemplo.dao.exception;

public class LlaveDuplicadaException extends Exception {

    public LlaveDuplicadaException(String mensaje) {
        super(mensaje);
    }

    public LlaveDuplicadaException() {
    }


}
