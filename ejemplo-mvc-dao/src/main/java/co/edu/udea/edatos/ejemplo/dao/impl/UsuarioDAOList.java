package co.edu.udea.edatos.ejemplo.dao.impl;

import co.edu.udea.edatos.ejemplo.dao.UsuarioDAO;
import co.edu.udea.edatos.ejemplo.dao.exception.LlaveDuplicadaException;
import co.edu.udea.edatos.ejemplo.model.Usuario;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class UsuarioDAOList implements UsuarioDAO {

    private static List<Usuario> bd = new ArrayList();

    @Override
    public Usuario guardarUsuario(Usuario usuarioNuevo) throws LlaveDuplicadaException {
        for (Usuario usuario : bd) {
            if (usuario.getIdentificacion() == usuarioNuevo.getIdentificacion()) {
                throw new LlaveDuplicadaException();
            }
        }
        bd.add(usuarioNuevo);
        return usuarioNuevo;
    }

    @Override
    public Optional<Usuario> consultarUsuarioPorCorreo(String correo) {
        for (Usuario usuario : bd) {
            if (usuario.getCorreo().equals(correo)) {
                return Optional.of(usuario);
            }
        }
        return Optional.empty();
    }

    @Override
    public Optional<Usuario> consultarUsuarioPorId(String id) {
        return Optional.empty();
    }

    @Override
    public List<Usuario> listarUsuarios() {
        return new ArrayList(bd);
    }
}
