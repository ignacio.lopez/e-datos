package co.edu.udea.edatos.ejemplo.dao;

import co.edu.udea.edatos.ejemplo.dao.exception.LlaveDuplicadaException;
import co.edu.udea.edatos.ejemplo.model.Usuario;

import java.util.List;
import java.util.Optional;

public interface UsuarioDAO {

    Usuario guardarUsuario(Usuario usuario) throws LlaveDuplicadaException;

    Optional<Usuario> consultarUsuarioPorCorreo(String correo);

    Optional<Usuario> consultarUsuarioPorId(String id);

    List<Usuario> listarUsuarios();
}
