package co.edu.udea.edatos.ejemplo.controller;

import co.edu.udea.edatos.ejemplo.bsn.DireccionBsn;
import co.edu.udea.edatos.ejemplo.bsn.UsuarioBsn;
import co.edu.udea.edatos.ejemplo.bsn.exception.BusinessException;
import co.edu.udea.edatos.ejemplo.model.Direccion;
import co.edu.udea.edatos.ejemplo.model.Usuario;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.util.List;
import java.util.Objects;

public class RegistrarDireccionController {

    @FXML
    private ComboBox<Usuario> cmbUsuarios;
    @FXML
    private TextField txtNombre;
    @FXML
    private TextField txtValor;
    @FXML
    private TableView<Direccion> tblDirecciones;
    @FXML
    private TableColumn<Direccion, String> clmIdentificacion;
    @FXML
    private TableColumn<Direccion, String> clmNombre;
    @FXML
    private TableColumn<Direccion, String> clmValor;

    private UsuarioBsn usuarioBsn = new UsuarioBsn();

    private DireccionBsn direccionBsn = new DireccionBsn();

    @FXML //postconstruct
    private void initialize() {
        List<Usuario> usuarios = this.usuarioBsn.listarUsuarios();
        ObservableList<Usuario> usuariosObservables = FXCollections.observableList(usuarios);

        this.cmbUsuarios.setItems(usuariosObservables);

    }


    public void cmbUsuarios_action(ActionEvent actionEvent){
        Usuario usuarioSeleccionado = cmbUsuarios.getValue();

        if(Objects.isNull(usuarioSeleccionado)){
            return;
        }

        //se consultan las direcciones que ya existen
        List<Direccion> direcciones = this.direccionBsn.listarDireccionesUsuario(usuarioSeleccionado.getIdentificacion());
        //se crea una lista observable para hacer binding con la tabla
        ObservableList<Direccion> direccionesObservables = FXCollections.observableList(direcciones);
        //se asigna la lista observable a la tabla
        tblDirecciones.setItems(direccionesObservables);
        //se especifican los métodos que deben invocarse para obtener los valores a mostrar en las celdas
        clmIdentificacion.setCellValueFactory(cellData->new SimpleStringProperty(Integer.toString(cellData.getValue().getIdUsuario())));
        clmNombre.setCellValueFactory(cellData->new SimpleStringProperty(cellData.getValue().getNombre()));
        clmValor.setCellValueFactory(cellData->new SimpleStringProperty(cellData.getValue().getValor()));

    }


    public void btnGuardar_clic() {
        int idSeleccionado = cmbUsuarios.getValue().getIdentificacion();
        String nombreIngresado=txtNombre.getText();
        String valorIngresado=txtValor.getText();

        //TODO validar campos: tipo de dato y valores requeridos.
        Direccion direccion = new Direccion(idSeleccionado, nombreIngresado, valorIngresado);
        try{
            Direccion direccionAlmacenada = direccionBsn.guardarDireccion(direccion);
            /*this.tblUsuarios.getItems().add(usuarioAlmacenado);*/
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Resultado");
            alert.setHeaderText("Guardar dirección");
            alert.setContentText("Dirección almacenada correctamente");
            this.limpiarFormulario();
            alert.showAndWait();
        }catch (BusinessException be){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Resultado");
            alert.setHeaderText("Guardar dirección");
            alert.setContentText(be.getMessage());
            alert.showAndWait();
        }

    }

    public void limpiarFormulario() {
        this.cmbUsuarios.setValue(null);
        this.txtNombre.setText("");
        this.txtValor.setText("");
    }

}
