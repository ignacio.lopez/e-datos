package co.edu.udea.edatos.ejemplo.bsn.exception.specific;

import co.edu.udea.edatos.ejemplo.bsn.exception.BusinessException;

public class IdentificacionInvalidaException extends BusinessException {
    public IdentificacionInvalidaException(String mensaje) {
        super(mensaje);
    }
}
