package co.edu.udea.edatos.ejemplo.model;

public class Direccion {

    //llave foránea a Usuario
    private int idUsuario;
    private String nombre;
    private String valor;

    public Direccion(){

    }

    public Direccion(int idUsuario, String nombre, String valor) {
        this.idUsuario = idUsuario;
        this.nombre = nombre;
        this.valor = valor;
    }

    public int getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }
}
