package co.edu.udea.edatos.ejemplo.bsn.exception.specific;

import co.edu.udea.edatos.ejemplo.bsn.exception.BusinessException;

public class CorreoInvalidoException extends BusinessException {

    public CorreoInvalidoException(String mensaje) {
        super(mensaje);
    }
}
