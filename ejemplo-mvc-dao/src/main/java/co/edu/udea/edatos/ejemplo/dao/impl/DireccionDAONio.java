package co.edu.udea.edatos.ejemplo.dao.impl;

import co.edu.udea.edatos.ejemplo.dao.DireccionDAO;
import co.edu.udea.edatos.ejemplo.dao.exception.LlaveDuplicadaException;
import co.edu.udea.edatos.ejemplo.model.Direccion;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SeekableByteChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static java.nio.file.StandardOpenOption.APPEND;

public class DireccionDAONio implements DireccionDAO {

    private final static int LONGITUD_REGISTRO = 80;
    private final static int LONGITUD_ID_USUARIO = 20;
    private final static int LONGITUD_NOMBRE = 20;
    private final static int LONGITUD_VALOR = 40;

    private final static String NOMBRE_ARCHIVO = "direcciones";

    private final static Path ARCHIVO = Paths.get(NOMBRE_ARCHIVO);

    public DireccionDAONio() {
        if (!Files.exists(ARCHIVO)) {
            try {
                Files.createFile(ARCHIVO);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
    }

    @Override
    public Direccion guardarDireccion(Direccion direccion) throws LlaveDuplicadaException {
        String registroDireccion = parseDireccion(direccion);
        byte[] datosRegistro = registroDireccion.getBytes();
        ByteBuffer byteBuffer = ByteBuffer.wrap(datosRegistro);
        try (FileChannel fileChannel = FileChannel.open(ARCHIVO, APPEND)) {
            fileChannel.write(byteBuffer);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return direccion;
    }

    @Override
    public List<Direccion> consultarDireccionesUsuario(int identificacionUsuario) {
        List<Direccion> direcciones = new ArrayList();
        try (SeekableByteChannel sbc = Files.newByteChannel(ARCHIVO)) {
            ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
            while (sbc.read(buffer) > 0) {
                buffer.rewind();
                CharBuffer registro = Charset.defaultCharset().decode(buffer);
                Direccion direccion = parseRegistro(registro);
                if (direccion.getIdUsuario() == identificacionUsuario) {
                    direcciones.add(direccion);
                }
                buffer.flip();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return direcciones;
    }


    private Direccion parseRegistro(CharBuffer registro) {
        Direccion direccion = new Direccion();

        String identificacion = registro.subSequence(0, LONGITUD_ID_USUARIO).toString().trim();
        direccion.setIdUsuario(Integer.parseInt(identificacion));
        registro.position(LONGITUD_ID_USUARIO);
        registro = registro.slice();

        String nombre = registro.subSequence(0, LONGITUD_NOMBRE).toString().trim();
        direccion.setNombre(nombre);
        registro.position(LONGITUD_NOMBRE);
        registro = registro.slice();

        String valor = registro.subSequence(0, LONGITUD_VALOR).toString().trim();
        direccion.setValor(valor);

        return direccion;
    }


    private String parseDireccion(Direccion direccion) {
        StringBuilder sb = new StringBuilder();
        sb.append(ajustarCampo(String.valueOf(direccion.getIdUsuario()), LONGITUD_ID_USUARIO))
                .append(ajustarCampo(direccion.getNombre(), LONGITUD_NOMBRE))
                .append(ajustarCampo(direccion.getValor(), LONGITUD_VALOR));
        return sb.toString();
    }

    private String ajustarCampo(String campo, int longitud) {
        if (campo.length() > longitud) {
            return campo.substring(0, longitud);
        }
        return String.format("%1$-" + longitud + "s", campo);
    }
}
