package co.edu.udea.edatos.ejemplo.dao.impl;

import co.edu.udea.edatos.ejemplo.dao.UsuarioDAO;
import co.edu.udea.edatos.ejemplo.dao.exception.LlaveDuplicadaException;
import co.edu.udea.edatos.ejemplo.model.Usuario;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.FileChannel;
import java.nio.channels.SeekableByteChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

import static java.nio.file.StandardOpenOption.APPEND;

public class UsuarioDAONio implements UsuarioDAO {

    private final static int LONGITUD_REGISTRO = 90;
    private final static int LONGITUD_ID = 20;
    private final static int LONGITUD_NOMBRES = 20;
    private final static int LONGITUD_APELLIDOS = 20;
    private final static int LONGITUD_CORREO = 30;

    private final static String NOMBRE_ARCHIVO = "usuarios";

    private final static Path ARCHIVO = Paths.get(NOMBRE_ARCHIVO);

    private final Map<String, Integer> indice = new HashMap();
    private static int direccion = 0;

    public UsuarioDAONio() {
        if (!Files.exists(ARCHIVO)) {
            try {
                Files.createFile(ARCHIVO);
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }
        crearIndice();
    }

    private void crearIndice() {
        System.out.println("Creando índice");
        try (SeekableByteChannel sbc = Files.newByteChannel(ARCHIVO)) {
            ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
            while (sbc.read(buffer) > 0) {
                buffer.rewind();
                CharBuffer registro = Charset.defaultCharset().decode(buffer);
                String id = registro.subSequence(0, LONGITUD_ID).toString().trim();
                System.out.println(String.format("%s -> %s", id, direccion));
                indice.put(id, direccion++);
                buffer.flip();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    @Override
    public Usuario guardarUsuario(Usuario usuario) throws LlaveDuplicadaException {
        String registroUsuario = parseUsuario(usuario);
        byte[] datosRegistro = registroUsuario.getBytes();
        ByteBuffer byteBuffer = ByteBuffer.wrap(datosRegistro);
        try (FileChannel fileChannel = FileChannel.open(ARCHIVO, APPEND)) {
            fileChannel.write(byteBuffer);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return usuario;
    }


    @Override
    public List<Usuario> listarUsuarios() {
        List<Usuario> usuarios = new ArrayList();
        try (SeekableByteChannel sbc = Files.newByteChannel(ARCHIVO)) {
            ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
            while (sbc.read(buffer) > 0) {
                buffer.rewind();
                CharBuffer registro = Charset.defaultCharset().decode(buffer);
                Usuario usuario = parseRegistro(registro);
                usuarios.add(usuario);
                buffer.flip();
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return usuarios;
    }

    private Usuario parseRegistro(CharBuffer registro) {
        Usuario usuario = new Usuario();

        String identificacion = registro.subSequence(0, LONGITUD_ID).toString().trim();
        usuario.setIdentificacion(Integer.parseInt(identificacion));
        registro.position(LONGITUD_ID);
        registro = registro.slice();

        String nombres = registro.subSequence(0, LONGITUD_NOMBRES).toString().trim();
        usuario.setNombres(nombres);
        registro.position(LONGITUD_NOMBRES);
        registro = registro.slice();

        String apellidos = registro.subSequence(0, LONGITUD_APELLIDOS).toString().trim();
        usuario.setApellidos(apellidos);
        registro.position(LONGITUD_APELLIDOS);
        registro = registro.slice();

        String correo = registro.subSequence(0, LONGITUD_CORREO).toString().trim();
        usuario.setCorreo(correo);

        return usuario;
    }

    @Override
    public Optional<Usuario> consultarUsuarioPorCorreo(String correo) {
        return Optional.empty();
    }

    @Override
    public Optional<Usuario> consultarUsuarioPorId(String id) {
        Integer direccionRegistro = indice.get(id);
        if (direccionRegistro == null) {
            System.out.println("El usuario no se encontró en el índice, por ende no existe en el archivo");
            return Optional.empty();
        }
        System.out.println("El usuario fue encontrado en el índice y se va a la dirección: " + direccionRegistro);
        System.out.println("(" + direccionRegistro * LONGITUD_REGISTRO + ")");
        try (SeekableByteChannel sbc = Files.newByteChannel(ARCHIVO)) {
            ByteBuffer buffer = ByteBuffer.allocate(LONGITUD_REGISTRO);
            sbc.position(direccionRegistro * LONGITUD_REGISTRO);
            sbc.read(buffer);
            buffer.rewind();
            CharBuffer registro = Charset.defaultCharset().decode(buffer);
            Usuario usuario = parseRegistro(registro);
            buffer.flip();
            return Optional.of(usuario);
        } catch (IOException ioe) {
            ioe.printStackTrace();
            return Optional.empty();
        }
    }

    private String parseUsuario(Usuario usuario) {
        StringBuilder sb = new StringBuilder();
        sb.append(ajustarCampo(String.valueOf(usuario.getIdentificacion()), LONGITUD_ID))
                .append(ajustarCampo(usuario.getNombres(), LONGITUD_NOMBRES))
                .append(ajustarCampo(usuario.getApellidos(), LONGITUD_APELLIDOS))
                .append(ajustarCampo(usuario.getCorreo(), LONGITUD_CORREO));
        return sb.toString();
    }

    private String ajustarCampo(String campo, int longitud) {
        if (campo.length() > longitud) {
            return campo.substring(0, longitud);
        }
        return String.format("%1$-" + longitud + "s", campo);
    }
}
