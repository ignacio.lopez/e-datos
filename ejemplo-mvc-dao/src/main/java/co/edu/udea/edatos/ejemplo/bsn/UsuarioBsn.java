package co.edu.udea.edatos.ejemplo.bsn;

import co.edu.udea.edatos.ejemplo.bsn.exception.BusinessException;
import co.edu.udea.edatos.ejemplo.bsn.exception.specific.CorreoInvalidoException;
import co.edu.udea.edatos.ejemplo.bsn.exception.specific.IdentificacionInvalidaException;
import co.edu.udea.edatos.ejemplo.dao.UsuarioDAO;
import co.edu.udea.edatos.ejemplo.dao.exception.LlaveDuplicadaException;
import co.edu.udea.edatos.ejemplo.dao.impl.UsuarioDAOList;
import co.edu.udea.edatos.ejemplo.dao.impl.UsuarioDAONio;
import co.edu.udea.edatos.ejemplo.model.Usuario;

import java.util.List;
import java.util.Optional;

public class UsuarioBsn {

    private UsuarioDAO usuarioDAO = new UsuarioDAONio();

    public Usuario guardarUsuario(Usuario usuario) throws BusinessException {
        //se valida la regla de negocio: no pueden existir dos correos iguales
        Optional<Usuario> usuarioOptional = usuarioDAO.consultarUsuarioPorCorreo(usuario.getCorreo());
        if(usuarioOptional.isPresent()){
            throw new CorreoInvalidoException("El correo electrónico ya estaba registrado previamente");
        }
        try{
            return usuarioDAO.guardarUsuario(usuario);
        }catch (LlaveDuplicadaException lde){
            throw new IdentificacionInvalidaException(String.format("Ya existe un usuario con la identificación que se ingresó: %s", usuario.getIdentificacion()));
        }
    }

    public List<Usuario> listarUsuarios(){
        //todo evaluar reglas de negocio a aplicar
        return this.usuarioDAO.listarUsuarios();
    }
}
