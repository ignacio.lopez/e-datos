package co.edu.udea.edatos.ejemplo.bsn;

import co.edu.udea.edatos.ejemplo.bsn.exception.BusinessException;
import co.edu.udea.edatos.ejemplo.dao.DireccionDAO;
import co.edu.udea.edatos.ejemplo.dao.exception.LlaveDuplicadaException;
import co.edu.udea.edatos.ejemplo.dao.impl.DireccionDAONio;
import co.edu.udea.edatos.ejemplo.model.Direccion;

import java.util.List;

public class DireccionBsn {

    private DireccionDAO direccionDAO;

    public DireccionBsn(){
        this.direccionDAO = new DireccionDAONio();
    }
    public Direccion guardarDireccion(Direccion direccion) throws BusinessException{
        try{
            return this.direccionDAO.guardarDireccion(direccion);
        }catch (LlaveDuplicadaException lde){
            lde.printStackTrace();
            throw new BusinessException("Error");
        }
    }

    public List<Direccion> listarDireccionesUsuario(int idUsuario){
        return this.direccionDAO.consultarDireccionesUsuario(idUsuario);
    }
}
