package co.edu.udea.edatos.ejemplo.dao;

import co.edu.udea.edatos.ejemplo.dao.exception.LlaveDuplicadaException;
import co.edu.udea.edatos.ejemplo.model.Direccion;

import java.util.List;

public interface DireccionDAO {

    Direccion guardarDireccion(Direccion direccion) throws LlaveDuplicadaException;

    List<Direccion> consultarDireccionesUsuario(int identificacionUsuario);

}
