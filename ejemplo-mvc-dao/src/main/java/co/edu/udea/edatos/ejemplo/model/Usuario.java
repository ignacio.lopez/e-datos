package co.edu.udea.edatos.ejemplo.model;

public class Usuario {
    private int identificacion; //20 caracteres
    private String nombres; //20 caracteres
    private String apellidos; //20 caracteres
    private String correo; //30 caracteres

    public Usuario() {

    }

    public Usuario(int identificacion, String nombres, String apellidos, String correo) {
        this.identificacion = identificacion;
        this.nombres = nombres;
        this.apellidos = apellidos;
        this.correo = correo;
    }

    public int getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(int identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String obtenerNombreCompleto() {
        return String.format("%s %s", nombres, apellidos);
    }

    @Override
    public String toString() {
        return identificacion+ " - "+ obtenerNombreCompleto();
    }
}
