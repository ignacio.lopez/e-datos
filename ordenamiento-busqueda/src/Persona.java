public class Persona implements Comparable<Persona> {
    private int id;
    private String nombres;

    public Persona(int id, String nombres) {
        this.id = id;
        this.nombres = nombres;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    /**
     * Compares this object with the specified object for order.  Returns a
     * negative integer, zero, or a positive integer as this object is less
     * than, equal to, or greater than the specified object.
     **/
    @Override
    public int compareTo(Persona o) {
        return this.id-o.id;
    }
}



