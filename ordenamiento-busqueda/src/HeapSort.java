public class HeapSort {

    private int[] maxHeapify(int datos[], int i, int n) {
        int left = 2 * i;                //left child
        int right = 2 * i + 1;          //right child
        int largest;
        if (left <= n - 1 && datos[left] > datos[i]) {
            largest = left;
        } else {
            largest = i;
        }
        if (right <= n - 1 && datos[right] > datos[largest]) {
            largest = right;
        }
        if (largest != i) {
            int aux = datos[i];
            datos[i] = datos[largest];
            datos[largest] = aux;
            datos = maxHeapify(datos, largest, n);
        }
        return datos;
    }

    private int[] buildMaxHeap(int datos[]) {
        for (int i = (datos.length - 1) / 2; i >= 1; i--) {
            datos = maxHeapify(datos, i, datos.length - 1);
        }
        return datos;
    }


    public int[] heapSort(int datos[]) {
        int heapSize = datos.length;
        datos = buildMaxHeap(datos);
        for (int i = datos.length - 1; i >= 2; i--) {
            int aux = datos[1];
            datos[1] = datos[i];
            datos[i] = aux;
            heapSize = heapSize - 1;
            datos = maxHeapify(datos, 1, heapSize);
        }
        return datos;
    }
}
