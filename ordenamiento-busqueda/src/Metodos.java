public class Metodos {


    public static Comparable[] bubbleSortP(Comparable[] arreglo) {
        for (int i = 0; i < arreglo.length; i++) {
            for (int j = 0; j < arreglo.length - i - 1; j++) {
                if (arreglo[j].compareTo(arreglo[j + 1]) < 0) {
                    Comparable aux = arreglo[j];
                    arreglo[j] = arreglo[j + 1];
                    arreglo[j + 1] = aux;
                }
            }
        }
        return arreglo;
    }


    public static int[] bubbleSort(int[] arreglo) {
        for (int i = 0; i < arreglo.length; i++) {
            for (int j = 0; j < arreglo.length - i - 1; j++) {
                if (arreglo[j] > arreglo[j + 1]) {
                    int aux = arreglo[j];
                    arreglo[j] = arreglo[j + 1];
                    arreglo[j + 1] = aux;
                }
            }
        }
        return arreglo;
    }

    public static int[] insertionSort(int[] arreglo) {
        for (int i = 1; i < arreglo.length; i++) {
            int aux = arreglo[i];
            int k = i - 1;
            boolean sw = false;
            while (!sw && k >= 0) {
                if (aux < arreglo[k]) {
                    arreglo[k + 1] = arreglo[k];
                    k--;
                } else {
                    sw = true;
                }
            }
            arreglo[k + 1] = aux;
        }
        return arreglo;
    }

    public static int[] selectionSort(int[] arreglo) {
        for (int i = 0; i < arreglo.length; i++) {
            int aux = arreglo[i];
            int k = i;
            for (int j = i + 1; j < arreglo.length; j++) {
                if (arreglo[j] < aux) {
                    aux = arreglo[j];
                    k = j;
                }
            }
            arreglo[k] = arreglo[i];
            arreglo[i] = aux;
        }
        return arreglo;
    }


    public static void main(String[] args) {
        int[] datos = {-1, 4, 3, 7, 1, 8, 5};

        HeapSort hs = new HeapSort();
        datos = hs.heapSort(datos);

        /*Persona[] personas = {
                new Persona(123, "a"),
                new Persona(12, "b"),
                new Persona(256, "c"),
                new Persona(321, "d"),
                new Persona(127, "e"),
                new Persona(1256, "q"),
                new Persona(1523, "z"),
                new Persona(162, "r"),
                new Persona(2056, "m")
        };

        personas = (Persona[])bubbleSortP(personas);*/

    }


}
